# qinglong_auto_push_ck
适配火狐浏览器的青龙面板自动上传ck脚本

目录结构：

| 文件名字 | 用途 |
|  ----  | ----  |
| main.py | 适配2.8版本的脚本 |  
| 2.2main.py | 适配2.2版本的脚本 |  
| ck.txt | 一行一个ck的文件 |   
| geckodriver.exe | 适配90.0.2 (64 位)火狐浏览器的driver |   
| cks_auto_select.py | ck去重小工具，用法自己悟 |   

上述文件需在同一文件夹里才可正常使用

下载环境:

cmd输入
```python
pip install selenium
```

修改main.py或2.2main.py的内容，填写用户名，密码，登陆地址(注意最后是以‘/’结束的)

ck形式，main.py或2.2main.py默认使用形式一，有需要的自己注释掉形式一使用形式二

形式一：

ck.txt里放入ck，一行一个

形式二：

ck1&ck2&ck3

在编辑器运行run一下就自动化操作了，当然也可以```python xxx.py```在命令行操作运行

ps:网速不好的加载不出来页面请自行调高操作间隔，我设置的都是0.几秒
